*&---------------------------------------------------------------------*
*&  Include           ZPOO_EJ001_TOP
*&---------------------------------------------------------------------*

CONSTANTS: gc_nom_ana    TYPE string VALUE 'Ana',
           gc_f_nac_ana  TYPE dats   VALUE '20040308',
           gc_nom_jose   TYPE string VALUE 'Jose',
           gc_f_nac_jose TYPE dats   VALUE '19690512'.

CLASS persona DEFINITION.

  PUBLIC SECTION.
    DATA: nombre TYPE string,
          f_nac  TYPE dats,
          edad   TYPE i.

    METHODS:
      nom_mays   IMPORTING i_nombre TYPE string
                 EXPORTING nombre   TYPE string,
      edad_anios IMPORTING f_nac TYPE dats
                 EXPORTING edad  TYPE n.

ENDCLASS.

DATA: go_persona_01 TYPE REF TO persona,
      go_persona_02 TYPE REF TO persona.
