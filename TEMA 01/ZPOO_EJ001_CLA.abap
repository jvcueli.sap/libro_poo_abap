*&---------------------------------------------------------------------*
*&  Include           ZPOO_EJ001_CLA
*&---------------------------------------------------------------------*

CLASS persona IMPLEMENTATION.

  METHOD nom_mays.
    me->nombre = to_upper( i_nombre ).
  ENDMETHOD.

  METHOD edad_anios.
    me->edad = sy-datum(4) - f_nac(4).
  ENDMETHOD.

ENDCLASS.
