*&---------------------------------------------------------------------*
*&  Include           ZPOO_EJ001_EVE
*&---------------------------------------------------------------------*
INITIALIZATION.
  CREATE OBJECT: go_persona_01,
                 go_persona_02.

START-OF-SELECTION.
  go_persona_01->nom_mays( i_nombre = gc_nom_ana ).
  go_persona_01->f_nac = gc_f_nac_ana.
  go_persona_01->edad_anios( f_nac = go_persona_01->f_nac ).

  go_persona_02->nom_mays( i_nombre = gc_nom_jose ).
  go_persona_02->f_nac = gc_f_nac_jose.
  go_persona_02->edad_anios( f_nac = go_persona_02->f_nac ).

END-OF-SELECTION.
  WRITE:/`Datos del objeto 1 (go_persona_01)`.
  WRITE: /.
  WRITE:/ |Nombre:           { go_persona_01->nombre }| .
  WRITE:/ |Fecha nacimiento: { go_persona_01->f_nac }|.
  WRITE:/ |Edad actual:      { go_persona_01->edad } |.
  ULINE.
  WRITE:/`Datos del objeto 2 (go_persona_02)`.
  WRITE: /.
  WRITE:/ |Nombre:           { go_persona_02->nombre }| .
  WRITE:/ |Fecha nacimiento: { go_persona_02->f_nac }|.
  WRITE:/ |Edad actual:      { go_persona_02->edad } |.
  ULINE.
