*&---------------------------------------------------------------------*
*& Report ZPOO_EJ001
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zpoo_ej001.

INCLUDE: zpoo_ej001_top, " Declaración de elementos globales
         zpoo_ej001_eve, " Registro de eventos de la aplicación
         zpoo_ej001_cla. " Implementación de clases
